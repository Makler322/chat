#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <pwd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>

#define ADDRESS "mysocket" // адрес для связи



typedef struct clientsID {
                            int id;
                            char *Name;
                            struct clientsID *private;
                            int NameSize;
                            struct clientsID *next;
                        }
        clientsID;
clientsID *idMain;

#define SIZE 16

typedef struct LIST {
                        char **lst;
                        int sizelist;
                        int curlist;
                    }
        LIST;

typedef struct BUF {
                        char *buf;
                        int curbuf;
                        int sizebuf;
                    }
        BUF;
LIST *listMain;
BUF *bufMain;
int c, connectionDescriptor, descriptor, globalFlagName, readingID, Adress;
unsigned short Port = 0;
clientsID *getter, *spisokP, *koliaska;

int getConnectionServer();
int reading();

void workServer();
void acceptMessage(int connectionDescriptor);
void fromListToCmd(int i, LIST *listMain);
void deleteFromList(int elem);
void appendToList(int elem, char *Name, int size);

void clearlist();
void nulllist();
void termlist();
void nullbuf();
void addsym();
void addword();

void start();
void word();
void newline();
void stop();


void deleteFromList(int elem){
    clientsID *spis = idMain;
    if (spis == NULL){
        perror("server:deleteFromList");
        return;
    }
    while(spis->next->id != elem){
        spis = spis->next;
    }
    clientsID *zveno = spis->next;
    spis->next = zveno->next;
    free(zveno);
}

void appendToList(int elem, char *Name, int size){
    clientsID *spis = idMain;
    while(spis->next != NULL){
        spis = spis->next;
    }
    spis->next = (struct clientsID*)malloc(sizeof(struct clientsID));
    spis->next->id = (int)malloc(sizeof(int));
    spis->next->id = elem;
    spis->next->Name = malloc(size);
    spis->next->Name = Name;
    spis->next->private = NULL;
    spis->next->NameSize = (int)malloc(sizeof(int));
    spis->next->NameSize = size;
    spis->next->id = elem;
    spis->next->next = NULL;
    return;
}

int getConnectionServer(){
    int len;
    struct sockaddr_in sa;
    // получаем свой сокет-дескриптор:
    if((descriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("client: socket");
        exit(1);
    }
    listMain = (LIST *)malloc(sizeof(LIST));
    bufMain = (BUF *)malloc(sizeof(BUF));
    nulllist();
    nullbuf();
    memset(&sa, '0', sizeof(sa));
    sa.sin_family = AF_INET;
    if (Port) {
        sa.sin_port = htons(Port);
    }
    else{
        sa.sin_port = htons(5000);
    }
    
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    // создаем адрес, c которым будут связываться клиенты
    
    // уничтожаем файл с именем ADDRESS, если он существует,
    // для того, чтобы вызов bind завершился успешно
    unlink(ADDRESS);
    //len = sizeof(sa.sun_family) + strlen(sa.sun_path);
    int uiop = 1;
    setsockopt(descriptor, SOL_SOCKET, SO_REUSEADDR, &uiop, sizeof(int));
    
    if (bind(descriptor, (struct sockaddr*)&sa, sizeof(sa)) < 0) {
        perror("server: bind");
        exit(1);
    }
    // слушаем запросы на сокет
    if (listen(descriptor, 128) < 0) {
        perror("server: listen");
        exit(1);
    }
    return descriptor;
}

int reading(){
    unsigned char symbolFromdes;
    int result;
    if (read(readingID, &symbolFromdes, sizeof(unsigned char))){
        result = symbolFromdes;
    }
    else{
        result = EOF;
    }
    return result;
}


void workServer(){
    FILE *fp;
    struct sockaddr_un ca;
    /* ------------------------------------------ */
    printf("Сервер готов к работе, клиентов пока нет\n");
    idMain = (struct clientsID*)malloc(sizeof(struct clientsID));
    idMain->id = (int)malloc(sizeof(int));
    idMain->id = descriptor;
    idMain->next = NULL;
    idMain->private = NULL;
    while(1){
        fd_set readfds;
        int fd;
        int max_d = descriptor;
        
        
        FD_ZERO(&readfds); 
        FD_SET(descriptor, &readfds);

        
        spisokP = idMain;
        while(spisokP != NULL){
            FD_SET(spisokP->id, &readfds);
            if (spisokP->id > max_d) max_d = spisokP->id;
            spisokP = spisokP->next;
        }

        select(max_d + 1, &readfds, 0, 0, 0);

        if (FD_ISSET(descriptor, &readfds)){ 
            socklen_t ca_len = sizeof(struct sockaddr_un);
            if ((connectionDescriptor = accept(descriptor, (struct sockaddr *)&ca, &ca_len)) < 0) {
                perror("server: accept");
                exit(1);
            }
            else {
                
                readingID = connectionDescriptor;
                c = reading();
                
                globalFlagName = 1;
                start();
                
            }          
        }
        spisokP = idMain;
        while(spisokP != NULL){
            if (FD_ISSET(spisokP->id, &readfds) && (spisokP->id != descriptor)){
                char symbolFromClient;
                int flagFirstSymbol = 1;
                while(1){
                    
                    if (read(spisokP->id, &c, sizeof(unsigned char)) != 0){
                        
                        readingID = spisokP->id;
                        start();
                        if (c == '\n'){

                            break;
                        }                     
                    }
                    else{
                        
                        getter = idMain;
                        while(getter != NULL){
                            if (getter->id == descriptor){
                                write(1, "Отключился пользователь ", 47);
                                write(1, spisokP->Name, spisokP->NameSize);
                            }
                            else if (getter->id != spisokP->id){
                                write(getter->id, "Отключился пользователь ", 47);
                                write(getter->id, spisokP->Name, spisokP->NameSize);
                            }
                            getter = getter->next;
                        }
                        deleteFromList(spisokP->id);
                        break;
                    }
                }
            }
            spisokP = spisokP->next;
        }
    }
}
/*---------------------- */
void fromListToCmd(int i, LIST *listMain){
    int flajok = 0, namePrivate = 0;
    while (i<(listMain->sizelist)-1){
        if (globalFlagName){
            globalFlagName = 0;
            char *Name;
            Name = NULL;
            while (i < (listMain->sizelist)-1){
                if (Name != NULL) {
                    
                    Name = realloc(Name, strlen(Name)+strlen((listMain -> lst)[i])+2);
                    strcat(Name, (listMain -> lst)[i]);
                    if (i == (listMain->sizelist)-2){
                        int nado = 1;
                        char *pp;
                        while (nado){
                            getter = idMain;
                            nado = 0;
                            while (getter != NULL){
                                if (getter->id != descriptor){
                                    pp = NULL;
                                    pp = realloc(pp, strlen(Name)+2);
                                    strcpy(pp, Name);
                                    strcat(pp, "\n");
                                    if (!strcmp(getter->Name, pp)){
                                        
                                        Name = realloc(Name, strlen(Name)+2);
                                        strcat(Name, "#");
                                        nado = 1;
                                        break;
                                    }
                                    free(pp);
                                }
                                getter = getter->next;
                            }
                        }
                        strcat(Name, "\n");
                    }
                    else{
                        strcat(Name, " ");
                    }                      
                }
                else{
                    
                    Name = realloc(Name, strlen((listMain -> lst)[i])+2);
                    strcpy(Name, (listMain -> lst)[i]);
                    if (i == (listMain->sizelist)-2){
                        int nado1 = 1;
                        char *pp1;
                        while (nado1){
                            getter = idMain;
                            nado1 = 0;
                            while (getter != NULL){
                                if (getter->id != descriptor){
                                    pp1 = NULL;
                                    pp1 = realloc(pp1, strlen(Name)+2);
                                    strcpy(pp1, Name);
                                    strcat(pp1, "\n");
                                    if (!strcmp(getter->Name, pp1)){
                                        
                                        Name = realloc(Name, strlen(Name)+2);
                                        strcat(Name, "#");
                                        nado1 = 1;
                                        break;
                                    }
                                    //free(pp);
                                }
                                getter = getter->next;
                            }
                        }
                        strcat(Name, "\n");
                    }
                    else{
                        strcat(Name, " ");
                    }                       
                }
                i++;
            }
            getter = idMain;
            while(getter != NULL){
                if (getter->id == descriptor){
                    write(1, "Подключился новый пользователь ", 60);
                    write(1, Name, strlen(Name));
                }
                else{
                    write(getter->id, "Подключился новый пользователь ", 60);
                    write(getter->id, Name, strlen(Name));
                }
                getter = getter->next;
            }
            appendToList(connectionDescriptor, Name, strlen(Name));
            spisokP = idMain;
            while(spisokP->next != NULL){
                spisokP=spisokP->next;
            }
        }
        else if (namePrivate){
            write(namePrivate, (listMain -> lst)[i], strlen((listMain -> lst)[i]));
            if (i == (listMain->sizelist)-2){
                write(namePrivate, "\n", 2);
            }
        }
        else if (flajok){
            flajok = 0;
            int Kostil = 1;
            getter = idMain;
            while(getter != NULL){
                if (getter->id != descriptor){
                    // Здесь запоняю приватных пользователей, да так, чтобы они не повторялись
                    char *message;
                    message = NULL;
                    message = realloc(message, strlen((listMain -> lst)[i])+2);
                    strcpy(message, (listMain -> lst)[i]);
                    strcat(message, "\n");
                    
                    char *message1;
                    message1 = NULL;
                    message1 = realloc(message1, strlen(getter->Name)+2);
                    strcpy(message1, " ");
                    strcat(message1, getter->Name);
                    
                    
                    if (!strcmp(message1, message)){
                        Kostil = 0;
                        namePrivate = getter->id;
                        write(namePrivate, "<private> ", 11);
                        write(namePrivate, spisokP->Name, spisokP->NameSize-1);
                        write(namePrivate, ": ", 3);
                    
                        koliaska = getter;
                        int danet = 1; //Заполняю в 1-2
                        //printf("----\n");
                        //printf("1) %d %d \n", spisokP->id, koliaska->id);
                        while (koliaska->private != NULL){
                            //printf("2) %d %d \n", spisokP->id, koliaska->id);
                        
                            if (koliaska->id == spisokP->id){
                                danet = 0;
                            }
                            koliaska = koliaska->private;
                        }
                        if (koliaska->id == spisokP->id){
                                danet = 0;
                            }
                        if (danet){
                            koliaska->private = (struct clientsID*)malloc(sizeof(struct clientsID));
                            koliaska->private->id = (int)malloc(sizeof(int));
                            koliaska->private->id = spisokP->id;
                            koliaska->private->Name = malloc(strlen(spisokP->Name));
                            koliaska->private->Name = spisokP->Name;
                            koliaska->private->private = NULL;
                        }
                        koliaska = spisokP;
                        int danet1 = 1; //Заполняю в 2-1
                        //printf("1) %d %d \n", koliaska->id, getter->id);
                        while (koliaska->private != NULL){
                            //printf("2) %d %d \n", koliaska->id, getter->id);
                            if (koliaska->id == getter->id){
                                danet1 = 0;
                            }
                            koliaska = koliaska->private;
                        }
                        if (koliaska->id == getter->id){
                                danet1 = 0;
                            }
                        if (danet1){
                            koliaska->private = (struct clientsID*)malloc(sizeof(struct clientsID));
                            koliaska->private->id = (int)malloc(sizeof(int));
                            koliaska->private->id = getter->id;
                            koliaska->private->Name = malloc(strlen(getter->Name));
                            koliaska->private->Name = getter->Name;
                            koliaska->private->private = NULL;
                        }
                        //printf("----\n");
                    }
                }
                getter = getter->next;
            }
            if (Kostil){
                write(spisokP->id, "Пользователя с таким именем нет\n", 60);
                break;
            }

        }
        else if ((!strcmp((listMain -> lst)[i], "\\users")) && (i==0)){
            
            getter = idMain;
            while(getter != NULL){
                if (getter->id != descriptor) {
                    write(spisokP->id, getter->Name, getter->NameSize);
                }
                getter = getter->next;
            }
            break;
        }
        else if ((!strcmp((listMain -> lst)[i], "\\private")) && (i==0)){
            flajok = 1;
        }
        else if ((!strcmp((listMain -> lst)[i], "\\privates")) && (i==0)){
            getter = spisokP;
            
            if (getter->private == NULL){
                
                write(spisokP->id, "Вы не состоите в приватных переписках\n", 71);
            }
            else{
                
                write(spisokP->id, "Список ваших приватных пользователей:\n", 72);
                while(getter->private != NULL){
                    write(spisokP->id, getter->private->Name, strlen(getter->private->Name));
                    getter = getter->private;
                }
            }
            
        }
        else {
            getter = idMain;
            while(getter != NULL){
                if (i == 0){
                    write(getter->id, spisokP->Name, spisokP->NameSize-1);
                    write(getter->id, ": ", 3);
                }
                write(getter->id, (listMain -> lst)[i], strlen((listMain -> lst)[i]));
                if (i == (listMain->sizelist)-2){
                    write(getter->id, "\n", 2);
                }
                getter = getter->next;
            }
        }
        i++;
    }
}
/* Task3 Task5*/
void start() {
    
    if (c==EOF){
        termlist();
        fromListToCmd(0, listMain);
        stop();
    }
    else if (c=='\n' || c == '\0' || c=='\r') {
        termlist();
        fromListToCmd(0, listMain);
        clearlist(); 
    }
    else
    {
        nullbuf();
        addsym();
        c=reading();
        word();
    }
}

void clearlist(){
    int i;
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    if ((listMain -> lst)==NULL) return;
    for (i=0; (listMain -> lst)[i]!=NULL; i++)
        free((listMain -> lst)[i]);
    free((listMain -> lst));
    (listMain -> lst)=NULL;
}

void nullbuf(){
    (bufMain -> buf)=NULL;
    (bufMain -> sizebuf)=0;
    (bufMain -> curbuf)=0;
}


//

void nulllist(){
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    (listMain -> lst)=NULL;
}

void termlist(){
    if ((listMain -> lst)==NULL) return;
    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)+1)*sizeof(*(listMain -> lst)));
    (listMain -> lst)[(listMain -> curlist)]=NULL;
    (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)=(listMain -> curlist)+1)*sizeof(*(listMain -> lst)));
}

void addsym(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=SIZE);
    (bufMain -> buf)[(bufMain -> curbuf)++]=c;
}

void addword(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=1);
    (bufMain -> buf)[(bufMain -> curbuf)++]='\0';
    (bufMain -> buf)=realloc((bufMain -> buf),(bufMain -> sizebuf)=(bufMain -> curbuf));

    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst), ((listMain -> sizelist)+=SIZE)*sizeof(*(listMain -> lst))); 
    (listMain -> lst)[(listMain -> curlist)++]=(bufMain -> buf);
        
}

void word(){
    if (c != '\n' && c != '\t' && c != ' '){
        addsym();
      
        c=reading();
      
        word();
    }
    else{
        addword();
        start();
    }
}

void newline(){
    clearlist();
    start();
}

void stop(){
    clearlist();
    free(listMain);
    free(bufMain);
    
    exit(0);
}

int main(int argc, char **argv){
    printf("Введите порт (0 иначе): ");
    scanf("%hu", &Port);
    printf("\n");
    sigaction(SIGPIPE, &(struct sigaction){SIG_IGN}, NULL);
    int descriptorServer = getConnectionServer();
    workServer(descriptorServer);
    return 0;
}